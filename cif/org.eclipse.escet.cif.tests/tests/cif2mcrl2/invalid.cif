//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2023 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

group g:
  event int[0..5] e1, e2;

  invariant 1 + 1 = 2;
  invariant 1 = 3;                  // Non-true invariant in group.

  initial 1 = 3;                    // Non-true initialization in group.

  automaton no_initial:             // No initial location.
    location:
      edge e1, e2, tau;             // Explicit 'tau'.
      edge when true;               // Implicit 'tau'.
  end

  automaton mult_initials:          // Multiple initial locations.
    location L0:
      initial;
    location L1:
      initial;
    location L2:
      initial;
  end

  plant p:
    disc int[1..2] x1 in {1,2};     // Non-deterministic variable init.
    disc int[1..2] x2 in any;       // Non-deterministic variable init.
    cont cnt der 1.0;               // Continuous variable, real type/value.
    disc list bool lb = [true];     // List type/expr.

    plant invariant 1 + 1 = 2;
    plant invariant 1 = 3;          // Non-true invariant in automaton.

    location:
      initial true;
      edge e1 do if true: x1 := 2 end; // 'if' update.
      edge e2 do lb := lb + [false];

      plant invariant 1+1 = 2;
      plant invariant 1 = 3;        // Non-true invariant in location.
  end

  cont x der 1;
  cont y;
  equation y' = 2;                  // Equation.
end
